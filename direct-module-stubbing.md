    The original author used the Example#2 to show a case, when, unlike in Example#1, you need a special library (specifically `mockery`) to be able to mock a module.
    Why do you need?

    **In the Example#1** `request` module was used as **a module exported an Object** - so it was simpler to stub it's properties with regular mocking library (`sinon`) means.
    **In the Example#2** `request` module was used as **a module exported a Function**, which you can't stub easy as you need the function owner (Object) to stub on.

    Just for fun I've added to the Example#2 one more test context, which solves the problem without `mockery`, _by use of exactly the same approach as in the Example#1 plus a little trick with require.cache_.

    This is the essence of the solution, by the way the original author explained it [at the end of the Example#1](http://bulkan-evcimen.com/testing_with_mocha_sinon):

    > In Node.js require(..) loads modules once into a cache. As our test case runs first it will loads the request module first. We use the reference to the module to be able to stub methods on it. Later on when we load getProfile module it will do a require('request') call which will retrieve the module from the cache with the get method stubbed out.

    P.S.
    I have to note that the demonstrated stub of require.cahce isn't comprehensive (for example it doesn't affect native modules like 'fs').
    In common case I recommend to use [mockery](https://www.npmjs.com/package/mockery) or [proxiquire](https://www.npmjs.com/package/proxyquire) etc., or at least something like [decache](https://github.com/dwyl/decache) instead of playing with require.cache manually.