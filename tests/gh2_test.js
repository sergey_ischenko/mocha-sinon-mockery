var expect = require('chai').expect
  , sinon = require('sinon')
  , mockery = require('mockery')
  , path = require('path');

describe('User Profile (gh2)', function(){
  var requestStub, getProfile;

  before(function(){
    mockery.enable({
      // warnOnReplace: false,
      // warnOnUnregistered: false,
      useCleanCache: true
    });

    requestStub = sinon.stub();

    // replace the module `request` with a stub object
    mockery.registerMock('request', requestStub);
    mockery.registerAllowable('../src/gh2');

    getProfile = require('../src/gh2');
  });

  after(function(){
    mockery.disable();
  });

  it('can get user profile', function(done){
    requestStub.yields(null, {statusCode: 200}, {login: "bulkan"});

    getProfile('bulkan', function(err, result){
      if(err) {
        return done(err);
      }
      expect(requestStub.called).to.be.equal(true);
      expect(result).to.not.be.empty;
      expect(result).to.have.property('login');
      done();
    });
  });
});

describe('User Profile (gh2) without mockery (direct stubbing the module in require.cache...)', function(){
  var requestStub, getProfile;
  var requestPath = require.resolve('request');

  before(function(){
    // Here we use the same approach as in gh1_test.js, but with a bit more tricky stubbing
    require('request'); // put the module into require.cache
    requestStub = sinon.stub(require.cache[requestPath], 'exports'); // stub the module exports in the cache
    getProfile = require('../src/gh2');
  });

  after(function(){
    require.cache[requestPath].exports.restore();
  });

  it('can get user profile', function(done){
    requestStub.yields(null, {statusCode: 200}, {login: "bulkan"});

    getProfile('bulkan', function(err, result){
      if(err) {
        return done(err);
      }
      expect(requestStub.called).to.be.equal(true);
      expect(result).to.not.be.empty;
      expect(result).to.have.property('login');
      done();
    });
  });
});
