var expect = require('chai').expect
  , sinon      = require('sinon');

var request    = require('request')
  , getProfile = require('../src/gh1');

describe('User Profile (gh1)', function(){
  before(function(done){
    sinon
      .stub(request, 'get')
      .yields(null, null, JSON.stringify({login: "bulkan"}));
    done();
  });

  after(function(done){
    request.get.restore();
    done();
  });

  it('can get user profile', function(done){
    getProfile('bulkan', function(err, result){
      if(err) return done(err);
      expect(request.get.called).to.equal(true);
      expect(result).to.not.be.empty;
      done();
    });
  });
});

// In Node.js require(..) loads modules once into a cache. As our test case runs first it will loads the request module first. We use the reference to the module to be able to stub methods on it. Later on when we load getProfile module it will do a require('request') call which will retrieve the module from the cache with the get method stubbed out.
