var request = require('request');

function getProfile(username, cb){
  request.get('https://api.github.com/users/' + username, function(err, response, body){
    if (err) return cb(err);
    cb(null, body);
  });
}

module.exports = getProfile;
