## Here are a bit customized examples of mocking modules from:

1) http://bulkan-evcimen.com/testing_with_mocha_sinon - here gh1.js and gh1_test.js.

2) http://bulkan-evcimen.com/using_mockery_to_mock_modules_nodejs.html - here gh2.js and gh2_test.js.

### What the customization:

* **Both examples are more similar one to other by which tools they use and how.**

    I removed somethuing excess (async, for example).

    Also I corrected both the examples to be using `mocha` + `chai.expect` + `sinon` the same way and they both run the same way, even together:

        npm run spec



* **I've added an experiment to avoid using `mockery` in Example#2 (as well as it is in Example#1).**

    I've made this in order to become aware of what is under the hood and to try to stub a module without use of any library.
    If you are interested in that - see details of my experiment in [direct-module-stubbing.md](./direct-module-stubbing.md).

